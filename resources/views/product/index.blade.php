@extends('layouts/layout')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Data Produk <small>{{$type}}</small>
			</h1>

			@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif
			
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			<ul id="submenu-container" class="nav nav-tabs" style="margin-bottom: 20px;">
				<li <?php if($active_tab == 'add') echo 'class="active"'; ?> ><a href="<?=url('/product/add-product/'.$uri4)?>" ><b>Tambah Produk</b></a></li>
				<li <?php if($active_tab == 'view_publish') echo 'class="active"'; ?> ><a href="<?=url('/product/view-product/'.$uri4.'/publish')?>" ><b>Data Produk (Publish)</b></a></li>
				<li <?php if($active_tab == 'view_unpublish') echo 'class="active"'; ?> ><a href="<?=url('/product/view-product/'.$uri4.'/unpublish') ?>" ><b>Data Produk (Unpublish)</b></a></li>
			</ul>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-plus"></i> {{$title}}
				</li>
			</ol>

		</div>
    </div>
	<div class="panel">
		@yield('panel-body')
	</div>
</div>

@endsection