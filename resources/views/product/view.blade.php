@extends('product/index')

@section('ext_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/jquery.dataTables.min.js" integrity="sha256-L4cf7m/cgC51e7BFPxQcKZcXryzSju7VYBKJLOKPHvQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/dataTables.bootstrap.min.js" integrity="sha256-lq/mLZPNqOQ0CHcWc0svPG23XfVdJTc4fhGCNr8lvag=" crossorigin="anonymous"></script>
@endsection
@section('ext_css')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/dataTables.bootstrap.min.css" integrity="sha256-PbaYLBab86/uCEz3diunGMEYvjah3uDFIiID+jAtIfw=" crossorigin="anonymous" />
@endsection

@section('panel-body')
<meta name="csrf-token" content="{{ csrf_token() }}">

<table id="grid-product" class="table table-hover table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Check</th>
            <th>Tanggal</th>
            <th>Nama Produk</th>
            <th>Kategori</th>
            <th>Foto</th>
            <th>No. Invoice</th>
            <th>Nama Supplier</th>
            <th>Deskripsi</th>
            <th>Actions</th>
        </tr>
    </thead>
</table>
@endsection


@section('page_script')

<script>
        $(document).ready(function(){
            $("#grid-product").dataTable({
                serverSide : true,
                processing : true,
                ajax        : {
                    url     : '{{url("/product/list")}}',
                    type    : "POST",
                    data    : function(e){
                        e.productType   = "{{$uri4}}";
                        e.status        = "{{$uri5}}";
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                "order": [[ 1, "desc" ]],
                columns : [
                    {
                        "data" : "check",
                        render : function(data,meta,row){
                            return '<input type="checkbox" class="item-check" data-product_id="'+row.id+'">';
                        },
                        className : "text-center",
                        orderable : false
                    },
                    { "data" : "datetime" },
                    { "data" : "name_item" },
                    { "data" : "category_id" },
                    { 
                        "data" : "image" ,
                        render : function(data){
                            if(data){
                                return '<img height=50 width=50 src="<?php echo asset("storage/app/images") ?>/'+data+'"></img>';
                            }else{
                                return '';
                            }
                        }
                    },
                    { "data" : "no_invoice" },
                    { "data" : "supplier_id" },
                    { "data" : "description" },
                    { 
                        "data" : "actions",
                        orderable : false,
                        render : function(data,type,row){
                            btn = '';
                            btn += '<a href="{{url("/product/edit")}}/'+row.id+'" class="btn btn-primary btn-sm"> Edit </a>';
                            btn += '<button class="btn btn-danger btn-sm btn-delete" data-id="'+row.id+'" >Delete</button>';
                            return btn;
                        },
                        className : 'text-center'
                    },
                ]
            });
        });
        $("#grid-product").on('click','.btn-delete',function(e){
            e.preventDefault();
            product_id  = $(this).data('id');
            confirm     = confirm("Apakah anda yakin ingin menghapus produk ini?");
            el = $(this);
            if(confirm){
                $.ajax({
                    url     : "{{url('product/delete')}}",
                    data    : {
                        id  : product_id
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type    : "post",
                    beforeSend : function(){
                        el.attr('disabled','true');
                    },
                    success : function(){
                        alert("Sukses menghapus produk");
                        $("#grid-product").DataTable().ajax.reload();
                    },
                    complete    : function(){
                        el.removeAttr('disabled');
                    }
                });
            }
        });
    </script>

@endsection