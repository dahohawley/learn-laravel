@extends('layouts.layout')

@section('content')
    <form role="form" action="<?= url('/product/save-edit') ?>" method="post" enctype="multipart/form-data" id="form-add-product" class="form-horizontal">
        {{ csrf_field() }}
        <input type="hidden" name="product_id" value="{{$product['id']}}">
        <input type="hidden" name="product_type" value="{{$product['product_type']}}">

        <div class="form-group">
            <label class="col-lg-2 control-label">Nama Item*</label>
            <div class="col-lg-8">
                <input type="text" value="{{$product['name_item']}}" id="name-item" name="name-item" class="form-control" title="masukkan nama produk anda" required>
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="col-lg-2 control-label">Kategori* : </label>
            <div class="col-lg-8">
                <select id="category-select"  class="form-control" name="category" title="pilih category product anda" required>
                    <option value="">Pilih Kategori Product Anda</option>
                    <option value="1" selected >Random Category</option>
                </select>
            </div>
        </div><br>
        <div class="form-group">
            <label class="col-lg-2 control-label">Harga Modal*</label>
            <div class="col-lg-8">
                <input type="number" value="{{$product['price_production']}}" id="harga-modal" name="harga-modal" class="form-control" title="masukkan harga modal anda" required>
            </div>
        </div><br>

        <?php $no = 1; foreach ($customerTypes as $row) { ?>

            {{-- Cari product pricenya dulu --}}
            <?php
                $ppPrice    = 0;
                $ppOldPrice = 0;
                $hide    = true;

                foreach($productPrices as $pp){
                    if($pp['cust_type_id'] == $row->id){
                        $ppPrice    = intval($pp['price']);
                        $ppOldPrice = intval($pp['old_price']);

                        if($ppOldPrice){
                            $hide       = false;
                        }
                    }
                }
            ?>

            <div class="form-group">
                <label class="col-lg-2 control-label">Harga Jual (<?= $row->name ?>)</label>
                <div class="col-lg-8">
                    <input type="text" value="{{$ppOldPrice}}" name="price_<?= $row->id ?>" id="price-<?=$no?>" class="price form-control" title="masukkan harga jual <?= $row->name ?> anda" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Harga Diskon (<?= $row->name ?>)</label>
                <div class="col-lg-8">
                    <input id="view_discount_price_<?= $row->id ?>" type="checkbox" <?php if(!$hide) : ?> checked <?php endif; ?> class="checkbox" name="view_discount_price_<?= $row->id ?>" value="1" style="display:inline-block; vertical-align:middle;">
                    <label for="<?= 'view_discount_price_' . $row->id ?>">Tampilkan Harga Diskon <?= $row->name ?>(<span style="color:red;">*jika tidak ada diskon lewati kolom ini</span>)</label><br/>
                <input type="text" value="{{$ppPrice}}" id="harga-diskon-<?= $row->id ?>" name="harga_diskon_<?= $row->id ?>" class="harga-diskon-<?= $no++ ?> form-control" title="masukkan harga diskon anda" <?php if($hide) :  ?> style="display: none;" <?php endif; ?>><br/>
                </div>
            </div>
        <?php } ?>

        <div class="form-group">
            <label class="col-lg-2 control-label">Harga Pembelian Grosir</label>
            <div class="col-lg-10">
                <div class="row">
                    <div class="pull-right" style="margin: 0px 20px 10px;">
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm" id="add-harga-grosir">
                            <i class="fa fa-plus"></i> Tambah Harga
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-grosir">
                            <thead>
                                <tr style="white-space: nowrap;">
                                    <th>Qty Awal</th>
                                    <th>Qty Akhir</th>
                                    <?php foreach($customerTypes as $row) { ?>
                                        <th>Harga <?= $row->name ?></th>
                                    <?php } ?>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($wholesalePrices as $wholesalePrice) : ?>
                                    <tr class="baris" id="baris-{{$wholesalePrice['id']}}">
                                        <td>
                                            <input type="number" name="qty_awal" class="form-control" value="{{$wholesalePrice['qty_awal']}}">
                                        </td>
                                        <td>
                                            <input type="number" name="qty_akhir" class="form-control" value="{{$wholesalePrice['qty_akhir']}}">
                                        </td>

                                        <?php foreach($customerTypes as $customerType) :  ?>
                                            {{-- Cari Valuenya --}}
                                            <?php
                                                $wPrice     = 0;
                                                foreach($wholesalePrices as $wp){
                                                    if($wp['cust_type_id'] == $customerType['id']){
                                                        $wPrice     = $wp['price'];
                                                    }
                                                }
                                            ?>
                                            <td><input type="text" accept="number" value="{{$wPrice}}" name="price_grosir_{{$customerType["id"]}}" class="form-control"></td>
                                        
                                        <?php endforeach ?>

                                        <td>
                                            <a href="javascript:void(0)" class="btn btn-danger btn-sm" id="hapus-baris-{{$wholesalePrice['id']}}"><i class="fa fa-times"></i> Hapus Harga</a>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Berat*</label>
            <div class="col-lg-8">
                <input type="text" id="berat-product" value="{{$product['weight']}}"  name="berat-product" class="form-control" title="masukkan berat produk anda" required><br/>
                <span>*Berat dalam format Kg<strong> (hanya angka saja)</strong>, contoh: 0.1</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Deskripsi</label>
            <div class="col-lg-8">
                <textarea id="deskripsi"  name="deskripsi" class="form-control" title="masukkan deskripsi produk anda" maxlength="500" required>{{$product['description']}}</textarea><br/>
                <span>*Maksimal menggunakan <strong>500 Karakter</strong></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Minimal Order*</label>
            <div class="col-lg-8">
                <input type="text" id="minimal-order" value="{{$product['min_order']}}" name="minimal-order" class="form-control" title="masukkan minimal order produk anda" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <div class="alert bg-warning" style="margin-bottom:0;">
                    <span>Gunakan Button dibawah ini untuk mengunggah Gambar. Note : hindari penggunaan karakter Titik ( . ) , Kurung () , dan Koma ( , ) pada nama File Gambar Anda, simbol yang kami sarankan antara lain <strong>Add @ , Strip - , dan Underscore _  </strong>. </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Upload Image</label>
            <div class="col-lg-8" id="foto">
                <input type="file" class="form-control" id="image" name="image[]" multiple accept="image/*">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Upload Video</label>
            <div class="col-lg-8">
                <input type="file" class="form-control" name="video" id="video-file" accept="video/*">
                <video controls class="m-t" id="video-preview" style="display: none; max-width: 400px; width: 100%"></video>
                <p class="help-block">Durasi video maksimal 30 detik</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Varian*</label>
            <div class="col-lg-8">
                <div class="variant-wrapper row">
                    <div class="col-md-12 text-right">
                        <button type="button" class="add-variant btn btn-info"><i class="fa fa-plus"></i> Tambah Varian</button>
                    </div>
                    <div class="col-md-8 variant-list">
                        <?php foreach($variants as $variant): ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <h5><strong>Isikan Varian Anda</strong></h5>
                                <input type="hidden" name="jml_input[]" id="jml_input">
                                <input type="text" required id="variant_1" name="variant_product[{{$variant['id']}}]" value="{{$variant['variant']}}" class="form-control m-b input-variant" title="silahkan masukkan variant produk anda">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <h5><strong>Isikan Jumlah Stock</strong></h5>
                                <input type="number" required id="stock_1" name="stock_product[{{$variant['id']}}]" value="{{$variant['stock']}}" class="form-control m-b input-stock" min="0">
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Tambahkan Harga Untuk Pajak</label>
            <div class="col-lg-8">
                <input id="status_harga_pajak" type="checkbox" class="checkbox" name="status_harga_pajak" value="1" style="display:inline-block; vertical-align:middle;">
                <label for="status_harga_pajak">Tambahkan harga untuk perkalian pajak</label><br/>
                <input type="number" id="harga_pajak" name="harga_pajak" class="form-control" style="display: none;">
                <br/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Bea Masuk</label>
            <div class="col-lg-8">
                <div class="input-group">
                    <input type="number" min="0" id="bea_masuk" value="{{$product['bea_masuk']}}" name="bea_masuk" class="form-control">
                    <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">PPN</label>
            <div class="col-lg-8">
                <div class="input-group">
                    <input type="number" min="0" id="ppn" value="{{$product['ppn']}}" name="ppn" class="form-control">
                    <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">PPH</label>
            <div class="col-lg-8">
                <div class="input-group">
                    <input type="number" value="{{$product['pph']}}" min="0" id="pph" name="pph" class="form-control">
                    <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Produk Tag : </label>
            <div class="col-lg-8">
                <?php foreach($tags as $tag) :  
                    $tagId      = $tag['id'];
                    $checked    = false;
                    foreach($selectedTags as $st){
                        if($st['tag_id'] == $tagId){
                            $checked = true;
                        }
                    }
                ?>
                    <label class="checkbox-inline">
                        <input type="checkbox" <?php if($checked): ?> checked <?php endif; ?>  name="tag[]" value="<?= $tag->id ?>"><?= $tag->name ?>
                    </label>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Status* : </label>
            <div class="col-lg-8">
                
                <select id="status-select"  class="form-control" name="status" title="pilih status product anda">
                    <option <?php if($product['status'] == 'Publish'):  ?> selected <?php endif; ?>     value="Publish">Publish</option>
                    <option <?php if($product['status'] == 'Unpublish'):  ?> selected <?php endif; ?>     value="Unpublish">Unpublish</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Notifikasi* : </label>
            <div class="col-lg-8">

                <select id="notif-select"  name="notif-select" class="form-control">
                    <option value="Tidak">Tidak</option>
                    <option value="Ya">Ya</option>
                </select>
                <textarea style="width: 75%; margin-top: 15px" rows="3" class="form-control hidden" id="value-notif" name="value_notif" placeholder="Text untuk notifikasi"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Supplier* : </label>
            <div class="col-lg-8">

                <select id="supplier" name="supplier" class="form-control">
                    <option value="">Pilih Supplier</option>
                    <option value="1">Test Supplier</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">No. Invoice*</label>
            <div class="col-lg-8">
                <input type="text" id="no_invoice" value=" {{$product['no_invoice']}} " name="no_invoice" class="form-control" title="Masukkan nomor invoice">
                <div class="row">
                    <label class="control-label col-md-3" style="text-align: left">Jenis Pembelian</label>
                    <div class="col-md-3">
                        <label class="radio-inline"><input type="radio" name="purchase_status" value="Cash">Cash</label>
                        <label class="radio-inline"><input type="radio" name="purchase_status" value="Kredit">Kredit</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="buttons-addproduct-wrapper">
            <input type="hidden" name="redirect_url" id="redirect_url"  value="" />
            <button type="submit" id="add-product" class="btn btn-lg btn-default m-b-mini">Save</button>
            <a href="#" id="add-back" class="btn btn-lg btn-default m-b-mini">Save and Back to List</a>
        </div>
    </form>
@endsection

@section('page_script')
	<script>

		$(document).ready(function(){
            $('#status_harga_pajak').click(function () {
                $('#harga_pajak').toggle(this.checked);
            });
			$('#add-harga-grosir').click(function() {
				var no = $('.baris').length + 1;
				var row = '<tr class="baris" id="baris-'+no+'">';
				row += '<td><input type="text" name="qty_awal[]" class="form-control"></td>';
                row += '<td><input type="text" name="qty_akhir[]" class="form-control"></td>';
                
				<?php foreach($customerTypes as $customerType) :  ?>

                    row += '<td><input type="text" name="price_grosir_{{$customerType["id"]}}" class="form-control"></td>';
                
                <?php endforeach ?>
                
				row += '<td><a href="javascript:void(0)" class="btn btn-danger btn-sm" id="hapus-baris-'+no+'"><i class="fa fa-times"></i> Hapus Harga</a></td></tr>';

				$('.table-grosir').append(row);

				$('#hapus-baris-'+no).click(function() {
					$('#baris-'+no).remove();
				});
			});
			
			$(".add-variant").click(function(){
				var no = $(".variant-list").length + 1;
				var content = '<div class="col-md-12 variant-list coloumn-'+no+'">';
				content += '<div class="row">';
				content +='<div class="col-md-4 col-sm-6 col-xs-6">';
				content +='<input type="hidden" name="jml_input[]">';
				content += '<input type="text" id="variant_'+no+'" name ="variant_product[]" class="form-control m-b input-variant" title="silahkan masukkan variant produk anda" >';
				content += '</div>';
				content += '<div class="col-md-4 col-sm-6 col-xs-6">';
				content += '<input type="number" id="stock_'+no+'" name="stock_product[]" class="form-control m-b input-stock" min="0">';
				content += '</div>';
				content +='<div class="col-md-4 col-sm-6 col-xs-6">';
				content += '<button type="button" class="delete-variant btn btn-danger m-b"><i class="fa fa-times"></i> Delete</a>';
				content += '</div>';
				content += '</div></div>';

				$(".variant-wrapper").append(content);

				$(".coloumn-"+no+" .delete-variant").click(function(){
					$(".coloumn-"+no).remove();
				});

				return false;
            });
            
            <?php foreach ($customerTypes as $row) { ?>
            $('#view_discount_price_<?= $row->id ?>').click(function () {
                $("#harga-diskon-<?= $row->id ?>").toggle(this.checked);
            });
            <?php } ?>
		});
	</script>
@endsection