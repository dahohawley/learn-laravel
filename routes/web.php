<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/product/add-product/{url4}','Product@create');
Route::get('/product/view-product/{url4}/{url5}','Product@view');
Route::get('/product/edit/{product_id}','Product@edit');
Route::post('/product/save-edit/','Product@saveEdit');
Route::post('/product/delete','Product@delete');
Route::post('/product/add-product/{url4}','Product@save');
Route::post('/product/list','Product@list');


