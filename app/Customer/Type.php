<?php
namespace App\Customer;
use Illuminate\Database\Eloquent\Model;
class Type extends Model{
    
    protected $primaryKey   = 'id';
    protected $table        = 'customer_type';
    public $incrementing    = true;

    
}
