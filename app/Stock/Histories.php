<?php

namespace App\Stock;

use Illuminate\Database\Eloquent\Model;

class Histories extends Model{
    protected $table    = 'stock_histories';
    
    /* di disable karena updated_at gaada kolomnya */
    public  $timestamps = false;
}
