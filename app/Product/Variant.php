<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model{
    protected $table    = 'product_variant';
    public $timestamps  = false;
    
}
