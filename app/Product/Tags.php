<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $table = 'product_tags';
    public $timestamps = false;
}
