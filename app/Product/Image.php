<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Image extends Model
{
    public $timestamps  = false;
    protected $table   = 'rel_produk_image';


    public function upload(UploadedFile $file, $prodId,$order = 0){
        $result         = new \stdClass;
            
        $originalName   = $file->getClientOriginalName();
        /* simpen filenya. */
        $file->storeAs('images',$originalName);

        /* Masukin ke db */
        $this->prod_id  = $prodId;
        $this->image    = $originalName;
        $this->urutan   = $order;
        
        if($this->save()){
            $result->code   = 0;
            $result->info   = "success uploading image";
            $result->data   = array(
                'fileName'  => $originalName,
                'imageId'   => $this->id
            );
        }else{
            $result->code   = 500;
            $result->info   = "failed saving image to db";
        }
        return $result;
    }
}
