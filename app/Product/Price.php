<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class Price extends Model{
    protected $guarded  = [];
    protected $table    = 'product_price';
    public $timestamps = false;
}
