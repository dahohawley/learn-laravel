<?php

namespace App\Wholesale;

use Illuminate\Database\Eloquent\Model;

class Price extends Model{
    public $timestamps  = false;
    protected $table    = 'harga_grosir';
    protected $fillable = [
        'prod_id',
        'qty_awal',
        'qty_akhir',
        'cust_type_id',
        'price'
    ];
    protected $attributes = [
        'price_lokal'   => 0,
        'price_luar'    => 0,
        'price'         => 0
    ];
}
