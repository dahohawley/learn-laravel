<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Request;
use App\Customer\Type as CustomerType;
use App\Product as AppProduct;
use App\Product\Image;
use App\Product\Variant;
use App\Purchase;
use App\Tags;
use App\Wholesale\Price;

class Product extends Controller{
    public function create($url4){
        $customerType   = new CustomerType;
        $customerType::all();
        $customerTypes  = $customerType::get();
        
        $tags           = new Tags();
        $tags::all();
        $tags           = $tags::get();
        
        return view('product/create',array(
            'uri4'              => $url4,
            'type'              => $this->parseProductType($url4),
            'tags'              => $tags,
            'active_tab'        => 'add',
            'title'             => 'Add Product '.$this->parseProductType($url4),
            'customerTypes'     => $customerTypes,
        ));
    }

    public function save($url4){
        $request    = Request::instance();
        $post       = $request->post();
        $product    = new \App\Product;

        $now                = date('Y-m-d H:i:s');

        /* Ambil post data */
            $nama_item          = $request->post('name-item');
            $category           = $request->post('category');
            $harga_modal        = $request->post('harga-modal');
            $diskon_checked     = $request->post('view_discount_price');
            $berat_produk       = $request->post('berat-product');
            $minimal_order      = $request->post('minimal-order');
            $deskripsi          = $request->post('deskripsi');
            $image_product      = $request->post('image_product');
            $qty_awal           = $request->post('qty_awal');
            $qty_akhir          = $request->post('qty_akhir');
            $price_lokal        = $request->post('price_lokal');
            $price_luar         = $request->post('price_luar');
            $promo              = $request->post('promo');
            $best_seller        = $request->post('best_seller');
            $status             = $request->post('status');
            $supplier           = $request->post('supplier');
            $no_invoice         = $request->post('no_invoice');
            $purchase_status    = $request->post('purchase_status');
            $status_harga_pajak = $request->post('status_harga_pajak');
            $harga_pajak        = $request->post('harga_pajak');
            $bea_masuk          = $request->post('bea_masuk');
            $ppn                = $request->post('ppn');
            $pph                = $request->post('pph');

        /* assign ke proprty produk */
            $product->datetime          = $now;
            $product->name_item         = $nama_item;
            $product->product_type      = 'Ready Stock';
            $product->category_id       = $category;
            $product->price_production  = $harga_modal;
            $product->weight            = $berat_produk;
            $product->description       = $deskripsi;
            $product->min_order         = $minimal_order;
            $product->status            = $status;
            $product->no_invoice        = $no_invoice;
            $product->supplier_id       = $supplier;
            $product->harga_pajak       = $status_harga_pajak == 1 ? $harga_pajak : 0;
            $product->bea_masuk         = $bea_masuk;
            $product->ppn               = $ppn;
            $product->pph               = $pph;
            $product->sku               = 0;
            $product->price_old         = 0;
            $product->price_old_luar    = 0;
            $product->price             = 0;
            $product->price_luar        = 0;
            $product->max_order         = 0;
            $product->supplier_id       = 0;
            $product->harga_pajak       = 0;
            $product->product_type      = $this->parseProductType($url4);
            $product->image             = '';

        $tags        = isset($post['tag']) ? $post['tag'] : array();
        $product->save();
        $product_id                 = $product->id;

        /* Upload Image */
        $uploadedImage              = array();
        if($request->hasFile('image')){
            $files   = $request->file('image');
            $imgOrder = 1;
            foreach($files as $file){
                $image              = new Image();
                $result             = $image->upload($file,$product_id,$imgOrder);
                $uploadedImage[]    = $result;
                $imgOrder++;
            }

            /* Update produk.image ganti valuenya jadi yang pertama di upload */
            $initialImage           = $uploadedImage[0];
            $fileName               = $initialImage->data['fileName'];
            $update                 = \App\Product::where('id',$product_id)
            ->update(array(
                'image'             => $fileName
            ));

        }else{
            $origName = '';
        }

        /* Upload video */
        $uploadedImage              = array();
        if($request->hasFile('video')){
            $file   = $request->file('video');
            $originalName   = $file->getClientOriginalName();
            $file->storeAs('videos',$originalName);
            
            /* Update produk.image ganti valuenya jadi yang pertama di upload */
            $update                 = \App\Product::where('id',$product_id)
            ->update(array(
                'video'             => $originalName
            ));

        }else{
            $origName = '';
        }
        /*Insert ke products tag */
        $savedTag   = [];
        foreach($tags as $tag){
            $productTag             = new \App\Product\Tags();
            $productTag->product_id = $product_id;
            $productTag->tag_id     = $tag;
            $productTag->save();
            $savedTag[]             = $productTag->id;
        }


        $jml            = $request->post('jml_input');
        $variants       = $request->post('variant_product');
        $stock_product  = $request->post('stock_product');
        
        $total_qty      = 0;

        /* insert variant & histories */
        $savedVariant   = array();
        $savedHistories = array();
        foreach($variants as $key => $variant){
            $mVariant               = new \App\Product\Variant;
            $mVariant->prod_id      = $product_id;
            $mVariant->variant      = $variant;
            $mVariant->available    = 'Tersedia';
            $mVariant->stock        = $stock_product[$key];
            
            if($mVariant->save()){
                $savedVariant[] = $mVariant->getAttributes();
                $variant_id     = $mVariant->id;
                $total_qty += $stock_product[$key];

                /* Insert stock histories */
                $histories              = new \App\Stock\Histories;
                $histories->prod_id     = $product_id;
                $histories->variant_id  = $variant_id;
                $histories->prev_stock  = 0;
                $histories->stock       = $stock_product[$key];
                $histories->qty         = $stock_product[$key];
                $histories->user_id     = 1; /* di set ke 1 karena gatau sessionnya ada dimana... */
                $histories->note        = 'Masuk Produk';
                $histories->created_at  = date('Y-m-d H:i:s');
                $savedHistories[]       = $histories->getAttributes();

            }
        }

        $customerType       = new \App\Customer\Type();
        $customerType::all();
        $customerTypes      = $customerType::get();

        /* Insert harga grosir */
        $savedWholesale     = array();
        if ($qty_awal != null || $qty_awal != '' || $qty_awal != 0) {
            for ($i = 0; $i < count($qty_awal); $i++) {
                foreach ($customerTypes as $row) {
                    if(!$price = $request->post('price_grosir_'.$row->id)){
                        $price[$i] = 0;
                    }
                    $wholesalePrice     = new \App\Wholesale\Price;
                    $wholesalePrice->fill(array(
                        'prod_id'      => $product_id,
                        'qty_awal'     => $qty_awal[$i],
                        'qty_akhir'    => $qty_akhir[$i],
                        'cust_type_id' => $row->id,
                        'price'        => $price[$i]
                    ));
                    if($wholesalePrice->save()){
                        $savedWholesale[]   = $wholesalePrice->getAttributes();
                    }
                }
            }
        }

        /* Insert Product Price */
        $savedProductPrice  = [];
        foreach ($customerTypes as $row) {
            $diskon = $request->post('harga_diskon_'.$row->id);
            if (!$diskon) {
                $price = $request->post('price_'.$row->id);
                $old_price = 0;
            } else {
                $price = $diskon;
                $old_price =  $request->post('price_'.$row->id);
            }
            $dataPrice = array(
                'prod_id'      => $product_id,
                'cust_type_id' => $row->id,
                'price'        => $price,
                'old_price'    => $old_price,
            );
            $productPrice       = new \App\Product\Price();
            $productPrice->fill($dataPrice);
            if($productPrice->save()){
                $savedProductPrice[] = $productPrice->getAttributes();
            }
        }

        /* Insert Purchase */
        $data_purchase  = [];
        if ($supplier > 0) {
            $data_purchase = array(
                'no_invoice'      => $no_invoice,
                'supplier_id'     => $supplier,
                'product_id'      => $product_id,
                'qty'             => $total_qty,
                'price'           => $harga_modal,
                'total'           => $total_qty * $harga_modal,
                'purchase_date'   => date('Y-m-d H:i:s'),
                'purchase_status' => $purchase_status,
                'payment_status'  => 'Belum Lunas',
                'user_id'         => 1
            );
            $purchases              = new Purchase();
            $purchases->fill($data_purchase);
            $purchases->save();
        }
        if ($request->post('this_redirect_url')) {
            return redirect($request->post('this_redirect_url'))->with('status','Produk anda berhasil dibuat');
        }
    }

    public function view($url4,$url5){
        if($url5 == 'publish'){
            $activeTab      = 'view_publish';
        }else{
            $activeTab      = 'view_unpublish';
        }
        
        $baseImgUrl         = url(\Storage::url('images'));

        return view('product/view',array(
            'uri4'              => $url4,
            'uri5'              => $url5,
            'active_tab'        => $activeTab,
            'type'              => $this->parseProductType($url4),
            'title'             => 'View Product '.$this->parseProductType($url4).' '.ucfirst($url5),
            'baseImgUrl'        => $baseImgUrl,
        ));
    }

    public function list(){
        $request    = Request::instance();
        $post       = $request->post();

        $offset      = $post['start'];
        $limit      = $post['length'];
        $orderCol   = $post['columns'][ $post['order'][0]['column'] ]['data'];
        $orderDir   = $post['order'][0]['dir'];
        
        $product    = new AppProduct();
        $builder    = $product->
            take($limit)
            ->skip($offset)
            ->orderBy($orderCol,$orderDir);
            
            $searchValue    = $post['search']['value'];
            if($searchValue){
                // $builder->where(function ($query) use(&$searchValue) {
                    $builder->whereRaw(
                        "
                        LOWER(name_item) LIKE '%".strtolower($searchValue)."%'
                        OR
                        LOWER(description) LIKE '%".strtolower($searchValue)."%'
                        "
                    );
                // });
            }
            
            $productType    = $this->parseProductType($post['productType']);
            $builder->where('product_type','=',$productType);
            $builder->whereRaw('lower(status) ="'.$post['status'].'" ');
            
            

        $products       = $builder->get();
        $totalRow       = $product->count();

        $list   = [];
        foreach($products as $product){
            $list[]     = $product->getAttributes();
        }

        $result     = array(
            'draw'  => $post['draw'],
            'data'  => $list,
            'recordsTotal' => $totalRow,
            'recordsFiltered' => $totalRow
        );

        return response()
            ->json($result)
            ->withCallback($request->input('callback'));

    }

    public function edit($product_id){
        $product        = AppProduct::whereId($product_id)->first()->getAttributes();
        $tags           = Tags::all();
        $customerTypes  = CustomerType::all();

        $productPrices  = [];
        $productPrice   = AppProduct\Price::whereProdId($product_id)->get();
        foreach($productPrice as $pp){
            $productPrices[]  = $pp->getAttributes();
        }

        /* Ambil harga grosir */
        $wholesalePrices    = [];
        $builder            = \App\Wholesale\Price::where('prod_id','=',$product_id)->get();       
        $builder->each(function($wholesalePrice) use(&$wholesalePrices){
            $wholesalePrices[] = $wholesalePrice->getAttributes();
        });

        /* Ambil Variant */
        $variants           = [];
        $builder            = \App\Product\Variant::where('prod_id','=',$product_id)->get();
        $builder->each(function($variant) use (&$variants){
            $variants[]     = $variant->getAttributes();
        });


        /* Ambil tag yang dipilih */
        $selectedTags       = [];
        $builder            = \App\Product\Tags::where('product_id','=',$product_id)->get();
        $builder->each(function($sTag) use (&$selectedTags){
            $selectedTags[]     = $sTag->getAttributes();
        });

        return view('product.edit',array(
            'uri4'              => '',
            'type'              => '',
            'tags'              => $tags,
            'active_tab'        => 'Edit',
            'title'             => 'Edit Product',
            'customerTypes'     => $customerTypes,
            'product'           => $product,
            'productPrices'     => $productPrices,
            'wholesalePrices'   => $wholesalePrices,
            'variants'          => $variants,
            'selectedTags'      => $selectedTags
        ));
    }

    public function saveEdit(){
        $request    = Request::instance();
        $post       = $request->post();
        $product_id = $post['product_id'];

        $customerTypes = \App\Customer\Type::all();

        /* Update Product */
        $bestSeller     = "Tidak";
        $promo          = "Tidak";
        foreach($post['tag'] as $tag){
            if($tag == '6'){
                $bestSeller = "Ya";
            }
            if($tag == '7'){
                $promo      = 'Ya';
            }
        }
        /* Update product price */
            foreach($customerTypes as $customerType){
                $updateData = [];
                $custId     = $customerType->id;
                $updateData['old_price']    = 0;
                $updateDate['price']        = 0;

                /* Assign Update Data */
                if(isset($post['price_'.$custId])){
                    $updateData['old_price']    = intval($post['price_'.$custId]);
                }
                if(isset($post['harga_diskon_'.$custId])){
                    $updateData['price']        = $post['harga_diskon_'.$custId];
                }

                /* Cari dulu udah ada atau belum datanya di db, kalo ada update, kalo gaada insert*/
                $builder    = \App\Product\Price::where('prod_id','=',$product_id)->where('cust_type_id',$custId)->first();
                if($builder->getAttributes()['id']){
                    $update     = \App\Product\Price::where('prod_id','=',$product_id)
                    ->where('cust_type_id',$custId)->update($updateData);
                }else{
                    $updateData['prod_id'] = $product_id;
                    $updateData['cust_type_id'] = $custId;
                    $price      = new \App\Product\Price;
                    $price->fill($updateData);
                    $price->save();
                }
            }

        /* Update harga grosir */
            foreach($customerTypes as $ct){
                $custId         = $ct->id;
                $qtyAwal        = $post['qty_awal'];
                $qtyAkhir       = $post['qty_akhir'];

                if(isset($post['price_grosir_'.$custId])){
                    $price      = $post['price_grosir_'.$custId];
                    $wpPrice    = new \App\Wholesale\Price();
                    $updateWp   = $wpPrice->where('prod_id','=',$product_id)->where('cust_type_id','=',$custId)->update(array(
                        'price'     => $price
                    ));
                }

            }
        /* Update variants*/
            $variant        = new Variant();
            $variants       = $variant->where('prod_id','=',$product_id)->get();
            $postVariants   = $post['variant_product'];
            foreach($variants as $var){
                $varId  = $var['id'];
                $exists = false;
                foreach($postVariants as $key => $pv){
                    if($key == $varId){
                        /* Update variant */
                        $updateVariant = Variant::where('id',$key)->update(array(
                            'variant'   => $pv,
                            'stock'     => $post['stock_product'][$key]
                        ));
                        continue;
                    }else{
                        $exists = false;
                    }
                }
                if(!$exists){
                    Variant::where('id',$key)->update(array(
                        'available'     => "Delete"
                    ));
                }
            }
        /* Create new variants kalo ada tambahan baru */
        
        // echo '<pre>'; print_r($postVariants); echo '</pre>';die();
        foreach($postVariants as $key => $pv){
            if($variants->count()){
                $createNew  = true;
                foreach($variants as $variant){
                    if($key == $variant->id){
                        $createNew = false;
                    }
                }
                if($createNew){
                    $variant            = new Variant();
                    $variant->prod_id   = $product_id;
                    $variant->stock     = $post['stock_product'][$key];
                    $variant->variant   = $pv;
                    $variant->save();
                }
            }else{
                /* Create new variants */
                $variant            = new Variant();
                $variant->prod_id   = $product_id;
                $variant->stock     = $post['stock_product'][$key];
                $variant->variant   = $pv;
                $variant->save();
            }
        }
        


        $dataProduct = array(
            'name_item' => $post['name-item'],
            'product_type' => $post['product_type'],
            'category_id' => $post['category'],
            'price_production' => $post['harga-modal'],
            'price_old' => 0,
            'price_old_luar' => 0,
            'price' => 0,
            'price_luar' => 0,
            'weight' => $post['berat-product'],
            'description' => $post['deskripsi'],
            'min_order' => $post['minimal-order'],
            'max_order' => 0,
            'best_seller' => $bestSeller,
            'promo' => $promo,
            'status' => $post['status'],
            'no_invoice' => $post['no_invoice'],
            'supplier_id' => ($post['supplier']) ? $post['supplier'] : 0,
            'harga_pajak' => ($post['harga_pajak']) ? $post['harga_pajak'] : 0,
            'bea_masuk' => $post['bea_masuk'],
            'ppn' => $post['ppn'],
            'pph' => $post['pph'],
        );
        $update = \App\Product::where('id',$product_id)->update($dataProduct);
        $product_type   = strtolower(str_replace(" ","_",$post['product_type']));
        $status         = $post['status'];
        return redirect(url("/product/view-product/$product_type/$status"))->with('status','Produk anda berhasil dirubah');
    }

    public function delete(){
        $request    = Request::instance();
        $post       = $request->post();
        $product_id = $post['id'];
        if(
        $update                 = \App\Product::where('id',$product_id)->update(array(
            'status'    => 'Delete'
        ))){
            $result     = array(
                'status'    => true
            );
        }else{
            $result     = array(
                'status'    => false
            );
        }

        return response()
            ->json($result)
            ->withCallback($request->input('callback'));
        
    }

    protected function parseProductType($type){
        switch($type){
            case "pre_order":
                return 'PO';
            break;
            case "ready_stock":
                return 'Ready Stock';
            break;
            default :
                return $type;
            break;
        }
    }
}